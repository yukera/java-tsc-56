package com.tsc.jarinchekhina.tm.command;

import com.tsc.jarinchekhina.tm.endpoint.ProjectDTO;
import com.tsc.jarinchekhina.tm.endpoint.ProjectEndpoint;
import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Getter
@Component
public abstract class AbstractProjectCommand extends AbstractCommand {

    @NotNull
    @Autowired
    protected ProjectEndpoint projectEndpoint;

    public void print(@NotNull final ProjectDTO project) {
        System.out.println("ID: " + project.getId());
        System.out.println("NAME: " + project.getName());
        System.out.println("DESCRIPTION: " + project.getDescription());
        System.out.println("STATUS: " + project.getStatus().value());
    }

}
