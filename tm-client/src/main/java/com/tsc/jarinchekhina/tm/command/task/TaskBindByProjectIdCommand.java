package com.tsc.jarinchekhina.tm.command.task;

import com.tsc.jarinchekhina.tm.command.AbstractTaskCommand;
import com.tsc.jarinchekhina.tm.util.TerminalUtil;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;

@Component
public final class TaskBindByProjectIdCommand extends AbstractTaskCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "task-bind-by-project-id";
    }

    @NotNull
    @Override
    public String description() {
        return "bind task to project by id";
    }

    @SneakyThrows
    @Override
    public void execute() {
        System.out.println("[BIND TASK TO PROJECT BY ID]");
        System.out.println("ENTER TASK ID:");
        @NotNull final String taskId = TerminalUtil.nextLine();
        System.out.println("ENTER PROJECT ID:");
        @NotNull final String projectId = TerminalUtil.nextLine();
        getTaskEndpoint().bindTaskByProjectId(serviceLocator.getSession(), taskId, projectId);
    }


}
