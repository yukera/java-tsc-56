package com.tsc.jarinchekhina.tm.command.user;

import com.tsc.jarinchekhina.tm.command.AbstractUserCommand;
import com.tsc.jarinchekhina.tm.endpoint.SessionDTO;
import com.tsc.jarinchekhina.tm.endpoint.SessionEndpoint;
import com.tsc.jarinchekhina.tm.exception.user.AccessDeniedException;
import com.tsc.jarinchekhina.tm.util.TerminalUtil;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public final class UserLoginCommand extends AbstractUserCommand {

    @NotNull
    @Autowired
    private SessionEndpoint sessionEndpoint;

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "login";
    }

    @NotNull
    @Override
    public String description() {
        return "login to system";
    }

    @SneakyThrows
    @Override
    public void execute() {
        System.out.println("[LOGIN]");
        System.out.println("ENTER LOGIN:");
        @NotNull final String login = TerminalUtil.nextLine();
        System.out.println("ENTER PASSWORD:");
        @NotNull final String password = TerminalUtil.nextLine();
        @Nullable final SessionDTO session = sessionEndpoint.openSession(login, password);
        if (session == null) throw new AccessDeniedException();
        serviceLocator.setSession(session);
    }

}
