package com.tsc.jarinchekhina.tm.endpoint;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each
 * Java content interface and Java element interface
 * generated in the com.tsc.jarinchekhina.tm.endpoint package.
 * <p>An ObjectFactory allows you to programatically
 * construct new instances of the Java representation
 * for XML content. The Java representation of XML
 * content can consist of schema derived interfaces
 * and classes representing the binding of schema
 * type definitions, element declarations and model
 * groups.  Factory methods for each of these are
 * provided in this class.
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _FindByLogin_QNAME = new QName("http://endpoint.tm.jarinchekhina.tsc.com/", "findByLogin");
    private final static QName _FindByLoginResponse_QNAME = new QName("http://endpoint.tm.jarinchekhina.tsc.com/", "findByLoginResponse");
    private final static QName _LockByLogin_QNAME = new QName("http://endpoint.tm.jarinchekhina.tsc.com/", "lockByLogin");
    private final static QName _LockByLoginResponse_QNAME = new QName("http://endpoint.tm.jarinchekhina.tsc.com/", "lockByLoginResponse");
    private final static QName _RemoveByLogin_QNAME = new QName("http://endpoint.tm.jarinchekhina.tsc.com/", "removeByLogin");
    private final static QName _RemoveByLoginResponse_QNAME = new QName("http://endpoint.tm.jarinchekhina.tsc.com/", "removeByLoginResponse");
    private final static QName _UnlockByLogin_QNAME = new QName("http://endpoint.tm.jarinchekhina.tsc.com/", "unlockByLogin");
    private final static QName _UnlockByLoginResponse_QNAME = new QName("http://endpoint.tm.jarinchekhina.tsc.com/", "unlockByLoginResponse");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.tsc.jarinchekhina.tm.endpoint
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link FindByLogin }
     */
    public FindByLogin createFindByLogin() {
        return new FindByLogin();
    }

    /**
     * Create an instance of {@link FindByLoginResponse }
     */
    public FindByLoginResponse createFindByLoginResponse() {
        return new FindByLoginResponse();
    }

    /**
     * Create an instance of {@link LockByLogin }
     */
    public LockByLogin createLockByLogin() {
        return new LockByLogin();
    }

    /**
     * Create an instance of {@link LockByLoginResponse }
     */
    public LockByLoginResponse createLockByLoginResponse() {
        return new LockByLoginResponse();
    }

    /**
     * Create an instance of {@link RemoveByLogin }
     */
    public RemoveByLogin createRemoveByLogin() {
        return new RemoveByLogin();
    }

    /**
     * Create an instance of {@link RemoveByLoginResponse }
     */
    public RemoveByLoginResponse createRemoveByLoginResponse() {
        return new RemoveByLoginResponse();
    }

    /**
     * Create an instance of {@link UnlockByLogin }
     */
    public UnlockByLogin createUnlockByLogin() {
        return new UnlockByLogin();
    }

    /**
     * Create an instance of {@link UnlockByLoginResponse }
     */
    public UnlockByLoginResponse createUnlockByLoginResponse() {
        return new UnlockByLoginResponse();
    }

    /**
     * Create an instance of {@link SessionDTO }
     */
    public SessionDTO createSessionDTO() {
        return new SessionDTO();
    }

    /**
     * Create an instance of {@link UserDTO }
     */
    public UserDTO createUserDTO() {
        return new UserDTO();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FindByLogin }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link FindByLogin }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.jarinchekhina.tsc.com/", name = "findByLogin")
    public JAXBElement<FindByLogin> createFindByLogin(FindByLogin value) {
        return new JAXBElement<FindByLogin>(_FindByLogin_QNAME, FindByLogin.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FindByLoginResponse }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link FindByLoginResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.jarinchekhina.tsc.com/", name = "findByLoginResponse")
    public JAXBElement<FindByLoginResponse> createFindByLoginResponse(FindByLoginResponse value) {
        return new JAXBElement<FindByLoginResponse>(_FindByLoginResponse_QNAME, FindByLoginResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LockByLogin }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link LockByLogin }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.jarinchekhina.tsc.com/", name = "lockByLogin")
    public JAXBElement<LockByLogin> createLockByLogin(LockByLogin value) {
        return new JAXBElement<LockByLogin>(_LockByLogin_QNAME, LockByLogin.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LockByLoginResponse }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link LockByLoginResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.jarinchekhina.tsc.com/", name = "lockByLoginResponse")
    public JAXBElement<LockByLoginResponse> createLockByLoginResponse(LockByLoginResponse value) {
        return new JAXBElement<LockByLoginResponse>(_LockByLoginResponse_QNAME, LockByLoginResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RemoveByLogin }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link RemoveByLogin }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.jarinchekhina.tsc.com/", name = "removeByLogin")
    public JAXBElement<RemoveByLogin> createRemoveByLogin(RemoveByLogin value) {
        return new JAXBElement<RemoveByLogin>(_RemoveByLogin_QNAME, RemoveByLogin.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RemoveByLoginResponse }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link RemoveByLoginResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.jarinchekhina.tsc.com/", name = "removeByLoginResponse")
    public JAXBElement<RemoveByLoginResponse> createRemoveByLoginResponse(RemoveByLoginResponse value) {
        return new JAXBElement<RemoveByLoginResponse>(_RemoveByLoginResponse_QNAME, RemoveByLoginResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link UnlockByLogin }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link UnlockByLogin }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.jarinchekhina.tsc.com/", name = "unlockByLogin")
    public JAXBElement<UnlockByLogin> createUnlockByLogin(UnlockByLogin value) {
        return new JAXBElement<UnlockByLogin>(_UnlockByLogin_QNAME, UnlockByLogin.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link UnlockByLoginResponse }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link UnlockByLoginResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.jarinchekhina.tsc.com/", name = "unlockByLoginResponse")
    public JAXBElement<UnlockByLoginResponse> createUnlockByLoginResponse(UnlockByLoginResponse value) {
        return new JAXBElement<UnlockByLoginResponse>(_UnlockByLoginResponse_QNAME, UnlockByLoginResponse.class, null, value);
    }

}
