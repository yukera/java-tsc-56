package com.tsc.jarinchekhina.tm.command.system;

import com.tsc.jarinchekhina.tm.command.AbstractCommand;
import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;

import java.util.Collection;

@Component
public final class CommandsCommand extends AbstractCommand {

    @NotNull
    @Override
    public String arg() {
        return "-сmd";
    }

    @NotNull
    @Override
    public String name() {
        return "system-commands";
    }

    @NotNull
    @Override
    public String description() {
        return "display list of commands";
    }

    @Override
    public void execute() {
        System.out.println("[COMMANDS]");
        @NotNull final Collection<String> commands = serviceLocator.getCommandService().getCommandNames();
        for (@NotNull final String command : commands) System.out.println(command);
    }

}
