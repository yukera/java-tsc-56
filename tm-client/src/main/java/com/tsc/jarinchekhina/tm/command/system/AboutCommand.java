package com.tsc.jarinchekhina.tm.command.system;

import com.jcabi.manifests.Manifests;
import com.tsc.jarinchekhina.tm.command.AbstractCommand;
import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;

@Component
public final class AboutCommand extends AbstractCommand {

    @NotNull
    @Override
    public String arg() {
        return "-a";
    }

    @NotNull
    @Override
    public String name() {
        return "system-about";
    }

    @NotNull
    @Override
    public String description() {
        return "display developer info";
    }

    @Override
    public void execute() {
        System.out.println("[ABOUT]");
        @NotNull final String developer = Manifests.read("developer");
        System.out.println("Developer: " + developer);
        @NotNull final String email = Manifests.read("email");
        System.out.println("E-mail: " + email);
    }

}
