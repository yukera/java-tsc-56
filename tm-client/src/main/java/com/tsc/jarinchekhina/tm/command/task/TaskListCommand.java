package com.tsc.jarinchekhina.tm.command.task;

import com.tsc.jarinchekhina.tm.command.AbstractTaskCommand;
import com.tsc.jarinchekhina.tm.endpoint.TaskDTO;
import com.tsc.jarinchekhina.tm.exception.entity.TasksListNotFoundException;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public final class TaskListCommand extends AbstractTaskCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "task-list";
    }

    @NotNull
    @Override
    public String description() {
        return "show task list";
    }

    @SneakyThrows
    @Override
    public void execute() {
        System.out.println("[LIST TASKS]");
        @Nullable List<TaskDTO> tasks = getTaskEndpoint().findAllTasks(serviceLocator.getSession());
        if (tasks == null) throw new TasksListNotFoundException();

        int index = 1;
        for (@NotNull final TaskDTO task : tasks) {
            @NotNull final String taskStatus = task.getStatus().value();
            System.out.println(index + ". " + task.getId() + ' ' + task.getName() + " (" + taskStatus + ")");
            index++;
        }
    }

}
