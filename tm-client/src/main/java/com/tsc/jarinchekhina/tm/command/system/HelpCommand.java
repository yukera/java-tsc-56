package com.tsc.jarinchekhina.tm.command.system;

import com.tsc.jarinchekhina.tm.command.AbstractCommand;
import com.tsc.jarinchekhina.tm.util.DataUtil;
import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;

import java.util.Collection;

@Component
public final class HelpCommand extends AbstractCommand {

    @NotNull
    @Override
    public String arg() {
        return "-h";
    }

    @NotNull
    @Override
    public String name() {
        return "system-help";
    }

    @NotNull
    @Override
    public String description() {
        return "display list of possible actions";
    }

    @Override
    public void execute() {
        System.out.println("[HELP]");
        @NotNull final Collection<AbstractCommand> commands = serviceLocator.getCommandService().getCommands();
        for (@NotNull final AbstractCommand command : commands) {
            @NotNull String result = "";
            if (!DataUtil.isEmpty(command.name())) result += command.name() + " ";
            if (!DataUtil.isEmpty(command.arg())) result += "(" + command.arg() + ") ";
            if (!DataUtil.isEmpty(command.description())) result += "- " + command.description();
            System.out.println(result);
        }
    }

}
