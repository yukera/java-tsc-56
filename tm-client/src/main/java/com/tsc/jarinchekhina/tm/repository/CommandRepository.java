package com.tsc.jarinchekhina.tm.repository;

import com.tsc.jarinchekhina.tm.api.repository.ICommandRepository;
import com.tsc.jarinchekhina.tm.command.AbstractCommand;
import com.tsc.jarinchekhina.tm.comparator.ComparatorByNameMethod;
import com.tsc.jarinchekhina.tm.util.DataUtil;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Repository
@NoArgsConstructor
public final class CommandRepository implements ICommandRepository {

    @NotNull
    private final List<AbstractCommand> commandList = new ArrayList<>();

    @NotNull
    private final Map<String, AbstractCommand> arguments = new LinkedHashMap<>();

    @NotNull
    private final Map<String, AbstractCommand> commands = new LinkedHashMap<>();

    @NotNull
    @Override
    public List<AbstractCommand> getCommandList() {
        return commandList.stream()
                          .sorted(ComparatorByNameMethod.getInstance())
                          .collect(Collectors.toList());
    }

    @NotNull
    @Override
    public Collection<AbstractCommand> getCommands() {
        return commands.values();
    }

    @NotNull
    @Override
    public Collection<AbstractCommand> getArguments() {
        return arguments.values();
    }

    @NotNull
    @Override
    public Collection<String> getCommandNames() {
        @NotNull final List<String> result = new ArrayList<>();
        for (@NotNull final AbstractCommand command : commands.values()) {
            @Nullable final String name = command.name();
            if (DataUtil.isEmpty(name)) continue;
            result.add(name);
        }
        return result;
    }

    @NotNull
    @Override
    public Collection<String> getCommandArgs() {
        @NotNull final List<String> result = new ArrayList<>();
        for (@NotNull final AbstractCommand command : arguments.values()) {
            @Nullable final String name = command.name();
            if (DataUtil.isEmpty(name)) continue;
            result.add(name);
        }
        return result;
    }

    @Nullable
    @Override
    public AbstractCommand getCommandByName(@NotNull final String name) {
        return commands.get(name);
    }

    @Nullable
    @Override
    public AbstractCommand getCommandByArg(@NotNull final String name) {
        return arguments.get(name);
    }

    @Override
    public void add(@NotNull final AbstractCommand command) {
        @Nullable final String arg = command.arg();
        @Nullable final String name = command.name();
        if (!DataUtil.isEmpty(arg)) arguments.put(arg, command);
        if (!DataUtil.isEmpty(name)) commands.put(name, command);
    }

}
