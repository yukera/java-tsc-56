package com.tsc.jarinchekhina.tm.command.system;

import com.jcabi.manifests.Manifests;
import com.tsc.jarinchekhina.tm.command.AbstractCommand;
import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;

@Component
public final class VersionCommand extends AbstractCommand {

    @NotNull
    @Override
    public String arg() {
        return "-v";
    }

    @NotNull
    @Override
    public String name() {
        return "system-version";
    }

    @NotNull
    @Override
    public String description() {
        return "display program version";
    }

    @Override
    public void execute() {
        System.out.println("[VERSION]");
        System.out.println(Manifests.read("build"));
    }

}
