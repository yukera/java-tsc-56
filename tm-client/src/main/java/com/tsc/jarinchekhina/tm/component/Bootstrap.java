package com.tsc.jarinchekhina.tm.component;

import com.tsc.jarinchekhina.tm.api.repository.ICommandRepository;
import com.tsc.jarinchekhina.tm.api.service.ICommandService;
import com.tsc.jarinchekhina.tm.api.service.IServiceLocator;
import com.tsc.jarinchekhina.tm.command.AbstractCommand;
import com.tsc.jarinchekhina.tm.endpoint.SessionDTO;
import com.tsc.jarinchekhina.tm.exception.system.UnknownCommandException;
import com.tsc.jarinchekhina.tm.repository.CommandRepository;
import com.tsc.jarinchekhina.tm.service.CommandService;
import com.tsc.jarinchekhina.tm.util.DataUtil;
import com.tsc.jarinchekhina.tm.util.TerminalUtil;
import lombok.Getter;
import lombok.Setter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Getter
@Setter
@Component
public final class Bootstrap implements IServiceLocator {

    @NotNull
    @Autowired
    private final ICommandRepository commandRepository = new CommandRepository();

    @NotNull
    @Autowired
    private final ICommandService commandService = new CommandService(commandRepository);

    @Nullable
    @Autowired
    private AbstractCommand[] abstractCommands;

    @Nullable
    private SessionDTO session = null;

    @SneakyThrows
    public void init() {
        initCommands(abstractCommands);
        start();
    }

    @SneakyThrows
    private void initCommands(@Nullable final AbstractCommand[] commandList) {
        if (commandList == null) throw new UnknownCommandException("(all)");
        for (@NotNull AbstractCommand command : commandList) initCommand(command);
    }

    private void initCommand(@NotNull final AbstractCommand command) {
        command.setServiceLocator(this);
        commandService.add(command);
    }

    @SneakyThrows
    private void start() {
        System.setProperty("com.sun.xml.ws.transport.http.client.HttpTransportPipe.dump", "true");
        System.setProperty("com.sun.xml.ws.transport.http.HttpAdapter.dump", "true");
        System.setProperty("com.sun.xml.internal.ws.transport.http.client.HttpTransportPipe.dump", "true");
        System.setProperty("com.sun.xml.internal.ws.transport.http.HttpAdapter.dump", "true");
        System.out.println("*** Welcome to Task Manager ***");
        while (true) {
            try {
                @NotNull final String command = TerminalUtil.nextLine();
                parseCommand(command);
                System.out.println("[OK]");
            } catch (Exception e) {
                System.err.println("[FAIL]");
            }
        }
    }

    @SneakyThrows
    public void parseCommand(@NotNull final String cmd) {
        if (DataUtil.isEmpty(cmd)) throw new UnknownCommandException(cmd);
        @Nullable final AbstractCommand command = commandService.getCommandByName(cmd);
        if (command == null) throw new UnknownCommandException(cmd);
        command.execute();
    }

}