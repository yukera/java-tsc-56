package com.tsc.jarinchekhina.tm.api.service;

import com.tsc.jarinchekhina.tm.dto.SessionDTO;
import com.tsc.jarinchekhina.tm.enumerated.Role;
import com.tsc.jarinchekhina.tm.model.User;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Collection;
import java.util.List;

public interface ISessionService {

    boolean checkDataAccess(@Nullable String login, @Nullable String password);

    @Nullable
    SessionDTO open(@Nullable String login, @Nullable String password);

    @Nullable
    SessionDTO sign(@Nullable SessionDTO session);

    boolean isValid(@Nullable SessionDTO session);

    void validate(@Nullable SessionDTO session);

    void remove(@NotNull SessionDTO session);

    void validate(@Nullable SessionDTO session, @Nullable Role role);

    @NotNull
    User getUser(@Nullable SessionDTO session);

    @NotNull
    String getUserId(@Nullable SessionDTO session);

    @NotNull
    List<SessionDTO> getListSession(@Nullable SessionDTO session);

    @SneakyThrows
    void update(@NotNull SessionDTO session);

    void add(@NotNull SessionDTO session);

    @NotNull List<SessionDTO> findAll();

    @NotNull
    SessionDTO findById(@NotNull String id);

    void removeById(@NotNull String id);

    void addAll(@NotNull Collection<SessionDTO> collection);

    void clear();

    void close(@Nullable SessionDTO session);

    void closeAll(@NotNull List<SessionDTO> sessionList);

}
