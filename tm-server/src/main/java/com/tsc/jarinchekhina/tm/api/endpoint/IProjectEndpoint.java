package com.tsc.jarinchekhina.tm.api.endpoint;

import com.tsc.jarinchekhina.tm.api.IEndpoint;
import com.tsc.jarinchekhina.tm.dto.ProjectDTO;
import com.tsc.jarinchekhina.tm.dto.SessionDTO;
import com.tsc.jarinchekhina.tm.enumerated.Status;
import com.tsc.jarinchekhina.tm.model.Project;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;

public interface IProjectEndpoint extends IEndpoint<Project> {

    void changeProjectStatusById(@Nullable SessionDTO session, @Nullable String id, @Nullable Status status);

    void changeProjectStatusByIndex(@Nullable SessionDTO session, @Nullable Integer index, @Nullable Status status);

    void changeProjectStatusByName(@Nullable SessionDTO session, @Nullable String name, @Nullable Status status);

    void clearProjects(@Nullable SessionDTO session);

    void createProject(@Nullable SessionDTO session, @Nullable String name);

    void createProjectWithDescription(
            @Nullable SessionDTO session,
            @Nullable String name,
            @Nullable String description);

    @NotNull
    List<ProjectDTO> findAllProjects(@Nullable SessionDTO session);

    @NotNull
    ProjectDTO findProjectById(@Nullable SessionDTO session, @Nullable String id);

    @NotNull
    ProjectDTO findProjectByIndex(@Nullable SessionDTO session, @Nullable Integer index);

    @NotNull
    ProjectDTO findProjectByName(@Nullable SessionDTO session, @Nullable String name);

    void finishProjectById(@Nullable SessionDTO session, @Nullable String id);

    void finishProjectByIndex(@Nullable SessionDTO session, @Nullable Integer index);

    void finishProjectByName(@Nullable SessionDTO session, @Nullable String name);

    void removeProjectById(@Nullable SessionDTO session, @Nullable String id);

    void removeProjectByIndex(@Nullable SessionDTO session, @Nullable Integer index);

    void removeProjectByName(@Nullable SessionDTO session, @Nullable String name);

    void startProjectById(@Nullable SessionDTO session, @Nullable String id);

    void startProjectByIndex(@Nullable SessionDTO session, @Nullable Integer index);

    void startProjectByName(@Nullable SessionDTO session, @Nullable String name);

    void updateProjectById(
            @Nullable SessionDTO session,
            @Nullable String id,
            @Nullable String name,
            @Nullable String description
    );

    void updateProjectByIndex(
            @Nullable SessionDTO session,
            @Nullable Integer index,
            @Nullable String name,
            @Nullable String description
    );

}
