package com.tsc.jarinchekhina.tm;

import com.tsc.jarinchekhina.tm.component.Bootstrap;
import com.tsc.jarinchekhina.tm.config.ServerConfig;
import org.jetbrains.annotations.NotNull;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class Application {

    public static void main(final String[] args) {
        @NotNull final AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(ServerConfig.class);
        final Bootstrap bootstrap = context.getBean(Bootstrap.class);
        bootstrap.init();
    }

}