package com.tsc.jarinchekhina.tm.service;

import com.tsc.jarinchekhina.tm.api.IService;
import com.tsc.jarinchekhina.tm.model.AbstractEntity;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;

@Service
@NoArgsConstructor
@AllArgsConstructor
public abstract class AbstractService<E extends AbstractEntity> implements IService<E> {

    @NotNull
    @Autowired
    protected ApplicationContext context;

}
